# irb(main):001:0> t = Taiyaki.new("あんこ")
# => #<Taiyaki:0x007f481274f9a8 @nakami="あんこ">
# irb(main):002:0> t.azi
# => "うまい"
# irb(main):003:0> t.nakami = "しお"
# => "しお"
# irb(main):004:0> t.azi
# => "まずい"
class Taiyaki
  attr_accessor :nakami

  def initialize(nakami)
    @nakami = nakami
  end

  def azi
    case @nakami
    when "あんこ"
      "うまい"
    when "しお"
      "まずい"
    end
  end
end
