# = 人間クラス
class Human
  MALE = 1
  FEMALE = 2
  GENDERS = { MALE => "男", FEMALE => "女" }

  # 性別、身長、体重、年齢
  attr_accessor :gender, :height, :weight, :age

  def initialize(gender:, height: 0, weight: 0, age: 0)
     raise "genderは1,2のいずれかを入力してください。" unless GENDERS.keys.include?(gender)
     @gender = gender
     @height = height
     @weight = weight
     @age = age
  end

  # == 男
  def male?
    gender == MALE
  end

  # == 女
  def female?
    !male?
  end

  # == 言語情報
  def language
    Language.new(lang)
  end
end