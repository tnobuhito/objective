# = 言語情報
require 'yaml'
class Language
  attr_reader :lang, :word

  def initialize(lang)
    @lang = lang
    @word = YAML.load_file(yml_path)
  end

  # == ymlのパス
  def yml_path
    File.expand_path("./language/#{lang}.yml")
  end

  # == 言葉を返す
  def [](key)
    word[lang]["language"][key]
  end
end