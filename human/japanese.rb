# = 日本人
class Human::Japanese < Human
  attr_accessor :first_name, :last_name

  def initialize(gender:, height: 0, weight: 0, age: 0, first_name: "", last_name: "")
    super(gender: gender, height: height, weight: weight, age: age)
    @first_name = first_name
    @last_name = last_name
  end

  # == 姓名
  def name
    last_name + first_name
  end

  # == 言語
  def lang
    "ja"
  end
end